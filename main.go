package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"ptapi/api"
	"ptapi/client"
	"ptapi/config"
)

var endClient string = "/api/client/servers/003f979a/resources"
var endApp string = "/api/application/servers/"

func main() {
	log.SetPrefix("ptAPI: ")
	log.SetFlags(0)

	// Configure
	cfg, err := ioutil.ReadFile("config.json")
	if err != nil {
		log.Fatal(err)
	}
	if !json.Valid([]byte(cfg)) {
		log.Fatal("Json is not valid")
	}
	config, err := config.Configure([]byte(cfg))
	if err != nil {
		log.Fatal(err)
	}

	// Main
	argLen := len(os.Args[1:])
	if argLen == 0 {
		arg := "help"
		help(arg)
	} else {
		arg := os.Args[1]
		if arg == "help" {
			help("help")
		}

		switch arg {
		case "raw":
			if len(os.Args) != 3 {
				help("ep")
			} else {
				ep := os.Args[2]
				if ep == "client" {
					//ops := os.Args[3]
					data, err := api.GET(endClient, config.KeyClient)
					if err != nil {
						log.Fatal(err)
					}
					outFile, err := os.Create("client.json")
					if err != nil {
						log.Fatal(err)
					}
					fmt.Fprint(outFile, string(data))
				} else if ep == "application" {
					//ops := os.Args[3]
					data, err := api.GET(endApp, config.KeyServer)
					if err != nil {
						log.Fatal(err)
					}
					outFile, err := os.Create("application.json")
					if err != nil {
						log.Fatal(err)
					}
					fmt.Fprint(outFile, string(data))
				}
			}
		case "backups":
			IDS, err := client.GETserverIDS()
			if err != nil {
				log.Fatal(err)
			}
			for i := 0; i < len(IDS); i++ {
				fmt.Printf("Found server: %s\n", IDS[i])
			}
		case "status":
			//IDS, err := client.GETserverIDS()
			if err != nil {
				log.Fatal(err)
			}

		default:
			fmt.Printf("Option %s not found\n", arg)
			help("help")
		}
	}
}

func help(cmd string) {
	switch cmd {
	case "help":
		log.Fatal("Options are [ help, raw]")
	case "ep":
		log.Fatal("API endpoint is required")
	}
}
