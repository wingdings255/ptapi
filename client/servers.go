package client

type usage struct {
	Object     string `json:"object"`
	Attributes struct {
		State     string `json:"current_state"`
		Suspended bool   `json:"is_suspended"`
		Resources struct {
			Mem   int64 `json:"memory_bytes"` //convert to human readable later
			CPU   int64 `json:"cpu_absolute"`
			Disk  int64 `json:"disk_bytes"`
			NetRx int64 `json:"network_rx_bytes"`
			NetTx int64 `json:"network_tx_bytes"`
		} `json:"resources"`
	} `json:"attributes"`
}

type database struct {
	Object string `json:"object"`
	Data   []db   `json:"data"`
}

type db struct {
	Object     string `json:"object"`
	Attributes struct {
		ID   string `json:"id"`
		Host struct {
			Address string `json:"address"`
			Port    int64  `json:"port"`
		} `json:"host"`
		Name       string `json:"name"`
		Username   string `json:"username"`
		Connect    string `json:"connections_from"`
		MaxConnect string `json:"max_connections"`
	} `json:"attributes"`
}

type file struct {
	Object     string `json:"object"`
	Attributes struct {
		Name     string `json:"name"`
		Mode     string `json:"mode"`
		Size     int64  `json:"size"` //Determine unit
		File     bool   `json:"is_file"`
		Symlink  bool   `json:"is_symlink"`
		Editable bool   `json:"is_editable"`
		Mimetype string `json:"mimetype"`
		Created  string `json:"created_at"`
		Modified string `json:"modifed_at"`
	} `json:"Attributes"`
}

type serverSchedule struct {
	Object     string `json:"server_schedule"`
	Attributes struct {
		ID   int64  `json:"id"`
		Name string `json:"name"`
		Cron struct {
			Day_of_week  string
			Day_of_month string
			Hour         string
			Minute       string
		} `json:"cron"`
		Active     bool   `json:"is_active"`
		Processing bool   `json:"is_processing"`
		LastRun    string `json:"last_run_at"`
		NextRun    string `json:"next_run_at"`
		Created    string `json:"created_at"`
		Updated    string `json:"updated_at"`
		Tasks      struct {
			Object string `json:"object"`
			Steps  []task `json:"data"`
		} `json:"tasks"`
	} `json:"attributes"`
}

type task struct {
	Object     string `json:"object"`
	Attributes struct {
		ID       int64  `json:"id"`
		Sequence int64  `json:"sequence_id"`
		Action   string `json:"action"`
		Payload  string `json:"payload"`
		Offset   int64  `json:"offset"`
		Queued   bool   `json:"is_queued"`
		Created  string `json:"created_at"`
		Updated  string `json:"updated_at"`
	} `json:"attributes"`
}

type subuser struct {
	Object     string `json:"object"`
	Attributes struct {
		UUID        string   `json:"uuid"`
		Username    string   `json:"username"`
		Email       string   `json:"email"`
		Image       string   `json:"image"`
		TwoFA       bool     `json:"2fa_enabled"`
		Created     string   `json:"created_at"`
		Permissions []string `json:"permissions"`
	}
}

type backups struct {
	Object string   `json:"object"`
	Data   []backup `json:"data"`
	Meta   struct {
		Pagination struct {
			Total       int64    `json:"total"`
			Count       int64    `json:"count"`
			PerPage     int64    `json:"per_page"`
			CurrentPage int64    `json:"current_page"`
			Pages       int64    `json:"total_pages"`
			Links       struct{} `json:"links"` //Docs dont explain type
		} `json:"pagination"`
	} `json:"meta"`
}

type backup struct {
	Object     string `json:"object"`
	Attributes struct {
		UUID         string   `json:"uuid"`
		Name         string   `json:"name"`
		IgnoredFIles []string `json:"ignored_files"` //docs dont spesify details
		Hash         string   `json:"sha256_hash"`
		Bytes        int64    `json:"bytes"`
		Created      string   `json:"created_at"`
		Completed    string   `json:"completed_at"`
	} `json:"attributes"`
}

type startup struct {
	Object string           `json:"object"`
	Data   []egg_varriables `json:"data"`
}
type egg_varriables struct {
	Object     string `json:"object"`
	Attributes struct {
		Name         string `json:"name"`
		Description  string `json:"description"`
		Env          string `json:"env_variable"`
		Default      string `json:"default_value"`
		Server_value string `json:"server_value"`
		Editable     bool   `json:"is_editable"`
		Rules        string `json:"rules"`
	} `json:"attribute"`
}

func GETbackup() (backups, error) {
	var data backups
	//ep := ""
	return data, nil
}
