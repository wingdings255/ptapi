package client

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"ptapi/api"
	"ptapi/config"
)

type servers struct {
	Object string   `json:"object"`
	Data   []server `json:"data"`
}

type server struct {
	Object     string `json:"object"`
	Attributes struct {
		Owner string `json:"server_owner"`
		ID    string `json:"identifier"`
		UUID  string `json:"uuid"`
		Name  string `json:"name"`
		Node  string `json:"node"`
		Sftp  struct {
			IP   string `json:"ip"`
			Port int64  `json:"port"`
		}
		Desc   string `json:"description"`
		Limits struct {
			Mem  int64 `json:"memory"`
			Swap int64 `json:"swap"`
			Disk int64 `json:"disk"`
			IO   int64 `json:"io"`
			CPU  int64 `json:"cpu"`
		} `json:"limits"`
		Feature_limits struct {
			DB         int64 `json:"databases"`
			Allocation int64 `json:"allocations"`
			Backups    int64 `json:"backups"`
		} `json:"feature_limits"`
		Suspended    bool `json:"is_suspended"`
		Installing   bool `json:"is_installing"`
		Relationship struct {
			Allocation struct {
				Object string        `json:"object"`
				Data   []allocations `json:"data"`
			} `json:"allocations"`
		} `json:"relationships"`
		Meta struct {
			Owner bool     `json:"is_server_owner"`
			Perms []string `json:"user_permissions"`
		} `json:"meta"`
	} `json:"attributes"`
}

type allocations struct {
	Object     string `json:"object"`
	Attributes struct {
		ID      int64  `json:"id"`
		IP      string `json:"ip"`
		Alias   string `json:"ip_alias"`
		Port    int64  `json:"port"`
		Notes   string `json:"notes"`
		Default bool   `json:"is_default"`
	}
}

func GETserverIDS() ([]string, error) {
	var ids []string
	var srvs servers

	ep := "/api/client"

	f, err := ioutil.ReadFile("config.json")
	if err != nil {
		return ids, errors.New("failed to read config file")
	}
	cfg, err := config.Configure([]byte(f))
	if err != nil {
		return ids, errors.New("failed to parse config file")
	}
	data, err := api.GET(ep, cfg.KeyClient)
	if err != nil {
		log.Fatal(err)
	}
	json.Unmarshal(data, &srvs)

	for i := 0; i < len(srvs.Data); i++ {
		ids = append(ids, srvs.Data[i].Attributes.ID)
	}

	return ids, nil
}

//func GETstatus(ids []string) (map[string]string, error) {}
