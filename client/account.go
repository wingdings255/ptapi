package client

type ACCdetails struct {
	Attributes struct {
		ID       int64  `json:"id"`
		Admin    bool   `json:"admin"`
		Username string `json:"username"`
		Email    string `json:"email"`
		FirstN   string `json:"first_name"`
		LastN    string `json:"last_name"`
		Lang     string `json:"language"`
	} `json:"attributes"`
}

type twoFA struct {
	Imgdat string `json:"image_url_data"`
}

type enable2fa struct {
	Attributes struct {
		Tokens []string `json:"tokens"`
	} `json:"attributes"`
}

type apiKeys struct {
	Data []apikey `json:"data"`
}
type apikey struct {
	Object     string `json:"object"`
	Attributes struct {
		ID      string   `json:"identifier"`
		Desc    string   `json:"description"`
		AllowIP []string `json:"allowed_ips"`
		LastUse string   `json:"Last_used_at"` //convert to unix at some point
		Created string   `json:"created_at"`
	} `json:"attributes"`
}

type keyCreate struct {
	Attributes struct {
		ID       string   `json:"identifier"`
		Desc     string   `json:"description"`
		AllowIPs []string `json:"allowed_ips"`
		Created  string   `json:"created_at"`
	} `json:"attributes"`
	Meta struct {
		Secret string `json:"secret_token"`
	} `json:"meta"`
}
