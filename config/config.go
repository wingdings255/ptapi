package config

import (
	"encoding/json"
	"errors"
)

type Configuration struct {
	KeyClient string `json:"user-key"`
	KeyServer string `json:"server-key"`
	Proto     string `json:"protocol"`
	Addr      string `json:"address"`
}

func Configure(cfg []byte) (Configuration, error) {

	if !json.Valid(cfg) {
		var config Configuration
		return config, errors.New("JSON data is not valid")
	} else {
		var config Configuration
		err := json.Unmarshal(cfg, &config)
		if err != nil {
			return config, errors.New("cant marshal JSON")
		}
		return config, nil
	}
}
