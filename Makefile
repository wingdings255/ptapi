TARGET	?=	ptapi

all: build

lint:
	go fmt $(go list ./... | grep -v /vendor/)
	go vet $(go list ./... | grep -v /vendor/)
build:
	go build -o $(TARGET)
