package api

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"ptapi/config"
)

func GET(endpoint string, key string) ([]byte, error) {
	f, err := ioutil.ReadFile("config.json")
	if err != nil {
		log.Fatal(err)
	}
	cfg, err := config.Configure([]byte(f))
	if err != nil {
		log.Fatal(err)
	}
	url := []string{cfg.Proto, "://", cfg.Addr, endpoint}

	api := strings.Join(url, "")
	fmt.Printf("Calling API at %s \n", api)

	// APi call
	req, err := http.NewRequest("GET", api, nil)
	if err != nil {
		var body []byte
		return body, errors.New("http error")
	}
	s := []string{"Bearer ", key}
	k := strings.Join(s, "")
	req.Header.Add("Authorization", k)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("HTTP status code: %s \n", res.Status)

	if res.StatusCode >= 400 {
		fmt.Println(string(body))
		return body, errors.New("400 status code")
	}
	return body, nil
}

func POST() ([]byte, error) {
	var data []byte
	return data, nil
}
